let x = 0; // Initialize x-coordinate for drawing rectangles
let y = 0; // Initialize y-coordinate for drawing rectangles
let spacing = 30; // Define the spacing between lines and size of rectangles

function setup() {
    createCanvas(990, 510); // Set up the canvas size to 1000x500 pixels
    background(255, 192, 203); // Set the background color to pink
    // Create horizontal and vertical grid lines
    for (let x = 0; x < width; x += spacing) {
        for (let y = 0; y < height; y += spacing) {
            stroke(255, 105, 180); // Set the line color to a shade of pink
            strokeWeight(1); // Set the thickness of the lines to 1 pixel
            line(x, 0, x, height); // Draw vertical lines
            line(0, y, width, y); // Draw horizontal lines
        }
    }
}

function draw() {
    // Set the fill color to a random shade of pink
    fill(random(200, 255), random(100, 150), random(100, 150));
    noStroke(); // Disable stroke for rectangles
    rect(x, y, spacing); // Draw a rectangle at (x, y) with size defined by spacing
    x += spacing; // Move x by 30 pixels
    // If x exceeds canvas width, reset x and move y to the next row
    if (x >= width) { 
        x = 0;
        y += spacing;
    }
    // If y exceeds canvas height, reset y to the top
    if (y >= height) {
        y = 0;
    }
}
