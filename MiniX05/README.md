## Generative art 

RunMe link: https://astrid-ap.gitlab.io/aestetisk-programmering/MiniX05

link to view my code: https://gitlab.com/astrid-ap/aestetisk-programmering/-/blob/main/MiniX05/sketchminix05.js?ref_type=heads

![](MiniX05video.mov)

### What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?

I wanted to create some sort of pink pattern and the rules I thought of for this were: 1. if done then start over 2 colors are random but within a pink range. When coding, the exact rules I implemented were: 


- After each rectangle is drawn, the x coordinate is incremented by the spacing value.

- If the x coordinate exceeds the canvas width, it is reset to 0, and the y coordinate is incremented by the spacing value.
- If the y coordinate exceeds the canvas height, it is reset to 0, starting the process over from the top left of the canvas.
- When the program reaches the bottom-right corner of the grid (width, height), it wraps around and starts drawing again from the top-left corner.
- The fill color of each square is randomly generated within a specified range for red, green, and blue channels. 

As the drawing progresses, the grid of squares repeats itself when reaching the end of the canvas, creating a continuous and repeating pattern.


#### References:
Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142 - chapter 5.

Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, (Cambridge, MA: MIT Press, 2012), 119-146. 

https://p5js.org/reference/#/p5/cos

https://p5js.org/reference/#/p5/sin

https://p5js.org/reference/#/p5/rotate




