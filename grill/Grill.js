let foodNames = ["pizza", "is", "pomfrit", "hotdog", "Dressing", "Sodavand"];
let foodImages = [];
let sizeOptions = ["Lille", "Mellem", "Stor"];
let pizzaOptions = ["Margerita", "Pepperoni", "Salat"];
let sodaOptions = ["Cola", "Faxe", "Fanta"];
let dressingOptions = ["Ketchup", "ChilliMayo", "Remoulade"];
let dropdowns = [];
let basket = [];

let pizzaImg, isImg, pomfritImg, hotdogImg, ketchupImg, colaImg;

function preload() {
  pizzaImg = loadImage("pizza.png");
  isImg = loadImage("Is.png");
  pomfritImg = loadImage("pomfrit.png");
  hotdogImg = loadImage("hotdog.png");
  ketchupImg = loadImage("ketchup.png");
  colaImg = loadImage("cola.png");
  
  foodImages = [pizzaImg, isImg, pomfritImg, hotdogImg, ketchupImg, colaImg];
}

function setup() {
  createCanvas(600, 900);
  noLoop();

  foodNames.forEach((food, i) => {
    let title = createP(food.charAt(0).toUpperCase() + food.slice(1));
    title.position(150, 115 + i * 100);
    title.style("font-size", "16px");
    title.style("margin", "0");

    let dropdown = createSelect();
    dropdown.position(150, 140 + i * 100);
    
    if (food === "pizza") {
      pizzaOptions.forEach(option => dropdown.option(option));
    } else if (food === "Sodavand") {
      sodaOptions.forEach(option => dropdown.option(option));
    } else if (food === "Dressing") {
      dressingOptions.forEach(option => dropdown.option(option));
    } else {
      sizeOptions.forEach(option => dropdown.option(option));
    }
    
    dropdowns.push(dropdown);

    let button = createButton("Tilføj");
    button.position(300, 140 + i * 100);
    button.mousePressed(() => addToBasket(food, dropdown.value()));
  });

  let basketTitle = createP("Kurv");
  basketTitle.position(50, 750);
  basketTitle.style("font-size", "20px");
  
  let basketList = createElement("ul");
  basketList.position(50, 780);
  basketList.id("basketList");

  let buyButton = createButton("Køb");
  buyButton.position(50, 850);
  buyButton.mousePressed(() => window.location.href = "thankyou.html");
}

function draw() {
  background(240);
  textAlign(CENTER, CENTER);
  textSize(18);
  text("DD's Grillbar", width / 2, 50);

  foodImages.forEach((img, i) => {
    image(img, 50, 100 + i * 100, 80, 80);
  });
}

function addToBasket(food, selection) {
  let item = `${food} - ${selection}`;
  basket.push(item);
  updateBasket();
}

function updateBasket() {
  let basketList = select("#basketList");
  basketList.html("");
  basket.forEach((item, index) => {
    let listItem = createElement("li", item);
    let removeButton = createButton("Fjern");
    removeButton.mousePressed(() => removeFromBasket(index));
    listItem.child(removeButton);
    basketList.child(listItem);
  });
}

function removeFromBasket(index) {
  basket.splice(index, 1);
  updateBasket();
}


