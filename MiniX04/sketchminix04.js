let img1;
let img2;

function preload() {
//1984
img1 = loadImage("https://openclipart.org/image/800px/239978");
// Eye outline
img2 = loadImage("https://images.squarespace-cdn.com/content/v1/604f0b54baa6172159798a3e/1616489230196-0JUDGD6JDCSIKLPICLGE/PRO_EYE_1.png");
}

function setup() {
  createCanvas(400, 400);
  background(210,58,55,255)
  image(img1, 125, 100, 150, 50);
  image(img2, 55, 140, 290, 200);
  
}
// Function to draw rotation objects
function drawRotationMarkers(x,y, radius) {
  noFill()
  stroke('black')
  // Draw a circle with twice the specified radius
  circle(x, y, 2*radius)

}
function rotateText(x, y, radius, textString) {
  drawRotationMarkers(x, y, radius)
  // Split the chars so they can be printed one by one
  let letters = textString.split("")
  // Decide an angle for the text
 textAlign(CENTER, BASELINE)
  textSize(25)
  fill('black')
  push()
  translate(x, y)  // move to the center of the circle
  for (let i = 0; i < letters.length; i++) {

      text(letters[i], 0, -radius)
      angleMode (DEGREES)
      rotate(6.6) // in degrees
  }
  pop()
}

function draw() {
  rotateText(200, 200, 150, "WAR IS PEACE FREEDOM IS SLAVERY IGNORANCE IS STRENGHT")

  //Camera drawing
  fill(0)
  ellipse(200, 240, 120, 120);
  fill(0,102,204) //blue
  ellipse(200, 240, 110, 110);
  fill(64,64,64) //grey
  ellipse(200, 240, 100, 100);

  // Calculate the center coordinates and diameter of the eye
  let cx = width/1.48;
  let cy = height/1.29;
  let d = 90; //diameter
  // Map the horizontal movement of the mouse to the x-coordinate of the iris within certain limits
  let x = map(mouseX, 0, width, cx-70 - d/6, cx-70 + d/6, true);
  // Map the vertical movement of the mouse to the y-coordinate of the iris within certain limits
  let y = map(mouseY, 0, height, cy-70 - d/6, cy-70 + d/6, true);

  noStroke();
  // "Eye ball" black
  fill(0);
  circle(cx-70, cy-70, d);
 
  // "Iris" dark red
  fill(153,0,0);
  circle(x, y, 35);
  
  // "Pupil" / smallest dot
  fill(255,0,0);
  circle(x, y, 20);

  // Write "2024" in each corner
  textSize(20);
  fill('black');
  textAlign(LEFT, TOP);
  text("2024", 10, 10);
  textAlign(RIGHT, TOP);
  text("2024", width - 10, 10);
  textAlign(LEFT, BOTTOM);
  text("2024", 10, height - 10);
  textAlign(RIGHT, BOTTOM);
  text("2024", width - 10, height - 10);
}