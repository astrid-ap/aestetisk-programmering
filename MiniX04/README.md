## 1984 or 2024?

RunMe link: https://astrid-ap.gitlab.io/aestetisk-programmering/MiniX04

link to view my code: https://gitlab.com/astrid-ap/aestetisk-programmering/-/blob/main/MiniX04/sketchminix04.js?ref_type=heads

![](MiniX04video.mov)

### Provide a title for and short the description of your work (1000 characters or less) as if you were going to submit it to the festival
My interactive artwork "1984 or 2024?" serves as a critical exploration of contemporary digital surveillance practices, drawing inspiration from George Orwell's work, "1984." Through a combination of visual elements and an interactive feature, users are invited to contemplate the pervasive nature of data capture in our society and its parallels with Orwell's dystopian vision. Inspired by the theme of "Capture All" from the transmediale open call, my piece prompts reflection on the implications of constant monitoring and the erosion of privacy within the digital world.

In the novel "1984," the government, personified by the oppressive regime of Big Brother, wields total control over its citizens through surveillance and manipulation of information. Written decades ago, the novel foresaw a future where privacy would be close to nonexistent. Fast forward to 2024, and Orwell's predictions seem more relevant than ever in our digital world.

Today users both willingly and unknowingly surrender vast amounts of personal data in exchange for convenience, connectivity, and entertainment. However, this exchange has profound implications for privacy and freedom. Companies and governments amass immense troves of data, enabling them to construct detailed profiles of individuals, track their behavior, and influence their choices. The parallels between Orwell's fictional dystopia and the realities of data capture in 2024 are striking. Like the citizens of Oceania, individuals today navigate a landscape where their every move is monitored, analyzed, and potentially exploited. The manipulation of information, the distortion of truth, and the erosion of privacy serve as stark reminders of the enduring relevance of "1984" in the digital age of today.

### Describe your program and what you have used and learnt.
I have incorporated mouse movement to animate an eye or a surveillance camera, representing the watchful gaze of the iconic "Big Brother" from Orwell's novel. 


In the Setup Function the background is set color to a deep red, and places two images on the canvas. One image is a representation of the year "1984”, and the other image depicts the outline of an eye

In the draw rotation markers function markers are drawn for rotation to indicate the rotation of text.

In the rotate text function it rotates and displays a text in a circular pattern

In the draw function the following tasks are performed:

- Rotates and displays the text "WAR IS PEACE FREEDOM IS SLAVERY IGNORANCE IS STRENGTH" in a circular pattern. This is a quote from 1984.

- It draws a representation of a camera/surveillance with circles and ellipses.

- Calculates and draws the iris and pupil of an eye, with the iris moving based on the mouse position, giving the impression of tracking.

    - The first map() function maps the horizontal movement of the mouse (mouseX) to the x-coordinate of the iris (x1). It constrains the iris movement within certain limits based on the width of the canvas.

    - The second map() function maps the vertical movement of the mouse (mouseY) to the y-coordinate of the iris (y). It constrains the iris movement within certain limits based on the height of the canvas.
- Writes the year "2024" in each corner of the canvas.

#### References:
Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119

https://p5js.org/reference/#/p5/textAlign

