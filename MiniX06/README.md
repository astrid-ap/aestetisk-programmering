## bacon pancakes makin bacon pancakes!!

RunMe link: https://astrid-ap.gitlab.io/aestetisk-programmering/MiniX06

link to view my code: https://gitlab.com/astrid-ap/aestetisk-programmering/-/blob/main/MiniX06/minix06sketch.js?ref_type=heads

![](MiniX06video.mov)

### Describe how does/do your game/game objects work?
My game is like a classic Space Invader game, but I turned into a game of “ghostbusting (;! The game revolves around a player controlling a ship or “ghostbuster” to shoot down ghosts descending from the top of the screen. The player can move the ship left and right, fire shots upwards, and has multiple lives. Ghosts move horizontally across the screen, changing direction when they reach the screen's edge, and occasionally fire laser shots downwards. The game keeps track of the player's score and highest score achieved in the session. When a player is hit by a ghost's laser, they lose a life, and the game ends when all lives are lost.

### Describe how you program the objects and their related attributes, and the methods in your game.
In my game, objects such as the player's ship, shots fired by the player, ghosts, and ghost lasers are implemented as classes. Each class encapsulates related attributes and methods. For example:
- The MyShip class handles the player's ship, including its position, dimensions, movement, and firing mechanism.

- The Shot class represents the shots fired by the player, managing their position, direction, length, and movement.

- The Ghost class defines the characteristics of each ghost, such as position, size, animation, and behavior.
- The Laser class manages the properties and movement of laser shots fired by ghosts.

### Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?
Object-oriented programming facilitates the organization and management of code by representing real-world entities as objects with attributes (properties) and behaviors (methods). This approach makes code easier to understand, maintain, and extend. Abstraction involves hiding complex implementation details behind simpler interfaces, allowing a focus on essential aspects while hiding unnecessary complexity. This enhances code readability and maintainability.



