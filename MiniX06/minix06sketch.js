let snake;
let scl = 20;
let food;
let gameOver = false;

function preload() {
  music = loadSound('song.mp3');
  foodImage = loadImage("https://static.vecteezy.com/system/resources/thumbnails/025/221/318/small_2x/pancake-dessert-bakery-ai-generate-png.png");
  jakeimg = loadImage('jake.png');
}

function setup() {
  createCanvas(400, 400);
  snake = new Snake();
  frameRate(10);
  pickLocation();
  music.loop()
}

function pickLocation() {
  let cols = floor(width / scl);
  let rows = floor(height / scl); 
  food = createVector(floor(random(cols)), floor(random(rows)));
  food.mult(scl);
}

function draw() {
  background('lightgreen');
  if (gameOver) {
    textAlign(CENTER, CENTER);
    textSize(32);
    fill(0);
    text("Game Over", width / 2, height / 2);
    image(jakeimg, 20,230,430,140);
    noLoop();
    return;
  }

  if (snake.eat(food)) {
    pickLocation();
  }

  snake.death();
  snake.update();
  snake.show();
  
  image(foodImage, food.x, food.y, scl, scl);
}

function keyPressed() {
  if (keyCode === UP_ARROW) {
    snake.direction(0, -1); 
  } else if (keyCode === DOWN_ARROW) {
    snake.direction(0, 1);
  } else if (keyCode === RIGHT_ARROW) {
    snake.direction(1, 0);
  } else if (keyCode === LEFT_ARROW) {
    snake.direction(-1, 0);
  }
}