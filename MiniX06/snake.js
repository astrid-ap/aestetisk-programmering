class Snake {
    constructor() {
      this.x = 0;
      this.y = 0;
      this.xspeed = 1;
      this.yspeed = 0;
      this.total = 0;
      this.tail = [];
    }
  
    eat(position) {
      let distance = dist(this.x, this.y, position.x, position.y);
      if (distance < 1) {
        this.total++;
        return true;
      } else {
        return false;
      }
    }
  
    direction(x, y) {
      if (this.xspeed !== -x && this.yspeed !== -y) {
      this.xspeed = x;
      this.yspeed = y; 
    }
  }
    death() {
      // Check if the snake hits itself
      for (let i = 0; i < this.tail.length; i++) {
        let position = this.tail[i];
        let distance = dist(this.x, this.y, position.x, position.y);
        if (distance < 1) {
          gameOver = true;
        }
      }
    }
  
    update() {
      for (let i = 0; i < this.tail.length - 1; i++) {
        this.tail[i] = this.tail[i + 1];
      }
      if (this.total >= 1) {
        this.tail[this.total - 1] = createVector(this.x, this.y);
      }
    
      this.x = this.x + this.xspeed * scl;
      this.y = this.y + this.yspeed * scl;
  
      this.x = constrain(this.x, 0, width - scl);
      this.y = constrain(this.y, 0, height - scl);
    }
  
    show() {
      fill(245, 187, 18);
      noStroke();
      for (let i = 0; i < this.tail.length; i++) {
        rect(this.tail[i].x, this.tail[i].y, scl, scl);
      }
      rect(this.x, this.y, scl, scl);
  
      if (this.xspeed > 0) {
        rect(this.x + 20, this.y + 0, 20, 5);
        rect(this.x + 20, this.y + 15, 20, 5);
      } else if (this.xspeed < 0) {
        rect(this.x + -20, this.y + 0, 20, 5);
        rect(this.x + -20, this.y + 15, 20, 5);
      } else if (this.yspeed < 0) {
        rect(this.x + 15, this.y + -20, 5, 20);
        rect(this.x + 0, this.y + -20, 5, 20);
      } else if (this.yspeed > 0) {
        rect(this.x + 15, this.y + 20, 5, 20);
        rect(this.x + 0, this.y + 20, 5, 20);
      }
  
      // Draw eyes
      ellipseMode(CENTER);
      if (this.xspeed > 0) {
        fill(0);
        ellipse(this.x + 15, this.y + 5, 8, 8);
        ellipse(this.x + 15, this.y + 15, 8, 8);
        fill(255);
        ellipse(this.x + 15, this.y + 5, 5, 5);
        ellipse(this.x + 15, this.y + 15, 5, 5);
      } else if (this.xspeed < 0) {
        fill(0);
        ellipse(this.x + 5, this.y + 5, 8, 8);
        ellipse(this.x + 5, this.y + 15, 8, 8);
        fill(255);
        ellipse(this.x + 5, this.y + 5, 5, 5);
        ellipse(this.x + 5, this.y + 15, 5, 5);
      } else if (this.yspeed < 0) {
        fill(0);
        ellipse(this.x + 5, this.y + 5, 8, 8);
        ellipse(this.x + 15, this.y + 5, 8, 8);
        fill(255);
        ellipse(this.x + 5, this.y + 5, 5, 5);
        ellipse(this.x + 15, this.y + 5, 5, 5);
      } else if (this.yspeed > 0) {
        fill(0);
        ellipse(this.x + 5, this.y + 15, 8, 8);
        ellipse(this.x + 15, this.y + 15, 8, 8);
        fill(255);
        ellipse(this.x + 5, this.y + 15, 5, 5);
        ellipse(this.x + 15, this.y + 15, 5, 5);
      }
  
      // Draw nose
      fill(0);
      ellipseMode(CENTER);
      if (this.xspeed > 0) {
        ellipse(this.x + 20, this.y + 10, 5, 5);
      } else if (this.xspeed < 0) {
        ellipse(this.x + 0, this.y + 10, 5, 5);
      } else if (this.yspeed < 0) {
        ellipse(this.x + 10, this.y + 0, 5, 5);
      } else if (this.yspeed > 0) {
        ellipse(this.x + 10, this.y + 20, 5, 5);
      }
    }
  }