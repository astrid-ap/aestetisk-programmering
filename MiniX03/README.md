## Oh no! The TV isn't working! What should I do while waiting for my laundry? :P

RunMe link: https://astrid-ap.gitlab.io/aestetisk-programmering/MiniX03

Link to view my code: https://gitlab.com/astrid-ap/aestetisk-programmering/-/blob/main/MiniX03/sketchminix03.js?ref_type=heads

#### Desbribe your throbber design, both conceptually and technically.
My throbber design consists of two main components: a spinning animation resembling a washing machine and a television with a throbber animation.
The washing machine spinner is made by a rotating arc, mimicking the motion of a spinning laundry machine.
The TV screen throbber is a circular animation of fading circles, commonly seen as a loading indicator.

![](MiniX03video.mov)

#### What do you want to explore and/or express?
My code explores the concept of temporality in digital culture by creating two animations that signify processes or waiting periods.

#### What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way?
In my program i use the `frameCount()` function to calculate the rotation angle of the throbber, allowing for smooth and continuous motion. This function counts the number of frames displayed since the program started, constructing a sense of time within the computational environment.

Additionally, I use a `for loop`. The loop iterates over a series of circles, each rotated to create the spinning effect. By controlling the loop's iterations and the degree of rotation for each circle, the animation appears "seamless". This construction of time within the loop mirrors the iterative nature of computational processes, where tasks are executed repeatedly until a condition is met.

Furthermore, I use the `rotate()` function to transform the position of elements within the animation. By applying rotational transformations, the throbber icon becomes animated.

#### Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?
Throbbler icons in digital culture typically signify loading or processing. This is captured in my code with the spinning animation and the fading circles, both of which are commonly associated with loading indicators. However, humans and machines perceive time differently. As humans we often perceive time subjectively, where waiting for a task to complete may feel longer or shorter depending on various factors such as engagement level, anticipation, or distractions, SO in the laundromat waiting for the laundry machine to finish might seem longer, as the TV is not working, so no distraction is provided. On the other hand, machines perceive time differently: They are following a predefined sequence of instructions without any subjective experience and the loading TV and the washing machine may by humans be perceived as periods of waiting time, while the machines perceive them as sequential steps to be executed. 

#### References
Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018)

Winnie Soon & Geoff Cox; 'Aesthetic Programming: A Handbook of Software Studies', 2020

https://p5js.org/reference/#/p5/arc

https://p5js.org/reference/#/p5/createButton

https://p5js.org/reference/#/p5/mouseIsPressed

https://p5js.org/reference/#/p5/rotate

https://p5js.org/reference/#/p5/translate

https://p5js.org/reference/#/p5/angleMode

https://p5js.org/reference/#/p5/degrees

https://p5js.org/reference/#/p5/frameCount

