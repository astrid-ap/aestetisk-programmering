let img1; // Declare variable for the image

//TV throbber
let throbberSpeed = 0; // Initial throbber speed

//Spinner washing machine
let spinnerSpeed = 100;

// Variable to track spinner state (running or not)
let isSpinnerRunning = false; 

function preload() {
  // Load the image before the setup() function runs
  img1 = loadImage("https://media.gettyimages.com/id/1204778591/photo/wooden-panelling.jpg?s=612x612&w=0&k=20&c=ikN8nBZEsvX_GzgKcPBX7u8I5gNUdKGl-PJl8k6NdOM=");
}

function setup() {
  createCanvas(1280, 710);
  background(img1); // Set the loaded image as the background

  //laundry basket
  let img2 = createImg("https://feedingmissouri.org/wp-content/uploads/2014/10/DirtyLaundry2-LaundryBasket.png");
  img2.position(300,45);
  img2.size(250,300)
  //plant
  let img3 = createImg ("https://www.kroger.com/product/images/large/back/0081247103733")
  img3.position(880,142);
  img3.size(170,220)
  //sign 1
  let img4 = createImg ("https://www.safetysign.com/images/source/large-images/E3009.png")
  img4.position(160,390);
  img4.size(75,54)
  //sign 2
  let img5 = createImg ("https://www.safetysign.com/images/source/large-images/E3009.png")
  img5.position(880,640);
  img5.size(75,54)
  //sign 3
  let img6 = createImg ("https://www.safetysign.com/images/source/large-images/E3009.png")
  img6.position(1115,400);
  img6.size(75,54)
  //pile of clothes
  let img7 = createImg ("https://static.vecteezy.com/system/resources/previews/028/199/200/original/pile-of-dirty-laundry-isolated-png.png")
  img7.position(1000,92);
  img7.size(300,300)
   //sign - laundry carts
   let img8 = createImg ("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRoA-mndDAKAlowGNT-AGREVNyjh7XZl2WQwJdmqqRj4M451U3skqXAC0R_B6nyvAnbLyU&usqp=CAU")
   img8.position(60,100);
   img8.size(120,100)

  // Create stop button - right
  let startButton = createButton('');
  startButton.position(575, 385);
  startButton.size(20, 20); // Set button size
  startButton.style('border-radius', '50%'); // Make button round
  startButton.style('background-color', 'rgb(43, 40, 40)'); // Set button color red
  startButton.mousePressed(startSpinner);

  // Create start button - left
  let stopButton = createButton('');
  stopButton.position(600, 385);
  stopButton.size(20, 20); // Set button size
  stopButton.style('border-radius', '50%'); // Make button round
  stopButton.style('background-color', 'rgb(46, 30, 30)'); // Set button color greyish
  stopButton.mousePressed(stopSpinner);
}

function draw() {
  drawShadow ()
  stroke(5)
  fill(172,160,100) //dark green
  rect(0, 340, 320, 420); //machine 1
  fill(207,202,164) //light green
  rect(320, 340, 320, 420); //machine2
  fill(172,160,100)
  rect(640, 340, 320, 420); //machine3
  rect(960, 340, 320, 420); //machine4
 
  //black screens on the machines
  fill(58,56,80)
  noShadow();
  rect(5, 360, 310, 70);//1
  rect(325, 360, 310, 70);//2
  rect(645, 360, 310, 70);//3
  rect(965, 360, 310, 70);//4

  //Fake buttons / Red or right side
  fill(46, 30, 30)
  ellipse(290, 395, 20, 20); //1
  ellipse(930, 395, 20, 20);//3
  ellipse(1250, 395, 20, 20);//4
  //Fake buttons 2 / Greyish or left side
  fill(43, 40, 40)
  ellipse(265, 395, 20, 20); //1
  ellipse(905, 395, 20, 20); //3
  ellipse(1225, 395, 20, 20); //4

  //Washing doors, grey outline
  drawShadow ()
  fill(135, 136, 138)
  ellipse(170, 560, 200, 200);
  ellipse(480, 560, 200, 200);
  ellipse(805, 560, 200, 200);
  ellipse(1125, 560, 200, 200);
  
  //Washing doors, black
  noShadow();
  fill(43, 40, 40)
  ellipse(170, 560, 150, 150);
  ellipse(480, 560, 150, 150);
  ellipse(805, 560, 150, 150);
  ellipse(1125, 560, 150, 150);
  
  //TV
  fill(33,33,32)
  rect(740, 0, 20, 50); //mount
  rect(600, 40, 300, 200); //black frame
  //The screen
  fill(70,80)
  rect(610, 50, 280, 180);

  //TV throbber
  noStroke();
  fill(250, 100);
  throbberSpeed += 3; // Increase throbber speed by 3 degrees per frame
  translate(width / 1.7, height / 5); // Move the origin to the center of the throbber
  angleMode(DEGREES); // Set angle mode to degrees instead of radians
  
  // Loop to create 8 circles in the throbber
  for (let i = 0; i < 8; i++) {
    // Calculate the rotation angle for each circle
    let throbberAngle = i * 45;
  
    push(); // Save the current transformation state
    rotate(throbberAngle + throbberSpeed); // Rotate by the calculated angle and the throbber speed
    translate(20, 0); // Move to the edge of the throbber radius
    circle(0, 0, 10); // Draw the circle at the current position
    pop(); // Restore the previous transformation state
  }

  // Washing machine Spinner
  if (isSpinnerRunning) {
    // Calculate the rotation index based on the frame count and spinner speed
    let rotationIndex = frameCount % spinnerSpeed; 
    // Calculate the increment angle for each step
    let angleIncrement = 360 / spinnerSpeed; 
    // Calculate the current angle for the spinner
    let angle = rotationIndex * angleIncrement; 

    push();
    translate(-273, 418);
    rotate(angle); // Rotate the spinner to the calculated angle
    noFill(); // Disable fill for the arc
    stroke(173, 216, 230);
    strokeWeight(15);
    arc(0, 0, 144, 144, 0, 270, OPEN);
    pop();
  }

//Give shadow
function drawShadow() {
  drawingContext.shadowOffsetX = 5;
  drawingContext.shadowOffsetY = 10;
  drawingContext.shadowBlur = 10;
  drawingContext.shadowColor = 'black';
}
// No shadow 
function noShadow() {
  drawingContext.shadowOffsetX = 0;
  drawingContext.shadowOffsetY = 0;
  drawingContext.shadowBlur = 0;
}

}
// Function to start the spinner animation
function startSpinner() {
  isSpinnerRunning = true;
}
// Function to stop the spinner animation
function stopSpinner() {
  isSpinnerRunning = false;
}