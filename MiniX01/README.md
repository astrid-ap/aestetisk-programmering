link to view my minix01: https://astrid-ap.gitlab.io/aestetisk-programmering/MiniX01

link to my code: https://gitlab.com/astrid-ap/aestetisk-programmering/-/blob/main/MiniX01/MiniX01sketch.js?ref_type=heads

### What have you produced?
I have produced a UFO hovering inspace with a beam of light extending from the UFO to the ground. It also includes a cute, cartoonish alien peering out of the UFO, and the background is speckled with randomly positioned stars.
##
Canvas Setup:

- The setup function initializes a 600x600 pixel canvas with a dark blue background, giving the appearance of a night sky. The frame rate is set to 10 frames per second to control the speed of the animation.

In draw

Ray of Light:

- A semi-transparent light beam is drawn from the UFO to the ground using a triangle with light colors and low opacity, creating a glowing effect.

UFO:
- The UFO is constructed with two ellipses: a larger dark grey ellipse for the body and a smaller one for the dome.

Window Frame:
- A grey ellipse acts as the window frame of the UFO.

Window:
- A light blue ellipse represents the window through which the alien can be seen.

Alien:
- A green ellipse inside the window symbolizes the alien's head.
The alien's face is detailed with black ellipses for the eyes and mouth.

Stars:
- Small, randomly positioned emoji stars are drawn on the canvas, each frame.
![](miniX01.png)

### How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?
I’ve enjoyed this coding experience, and I’ve found the references to be immensely helpful. The ones I’ve looked at also appear understandable to me. It has also been really fun so far and I’m excited to get better. 

### How is the coding process different from, or similar to, reading and writing text?
Similar to reading text, coding involves interpreting and understanding. In terms of writing, coding requires a precise and structured approach unlike "text writing," where ambiguity may be valued at times. Although both follow syntax rules. Another thing I notice is that for example when writing in Google Docs, the program will tell me if I misspell something, whereas coding programs are not exactly as helpful when making a mistake.

### What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?
To me, coding and programming are creative expressions that I’m excited to get better at. It’s also a lot of problem-solving, which can be frustrating but giving once solved. The reading refreshes terms we’ve learned in the previous lessons, making some of them clearer to me.


### References
https://p5js.org/reference/#/p5/ellipse

https://p5js.org/reference/#/p5/random

https://p5js.org/reference/#/p5/triangle

Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
