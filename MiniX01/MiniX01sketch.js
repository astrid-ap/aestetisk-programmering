
function setup() {
 createCanvas(600, 600);
 background(25,25,50);
 frameRate(10);
}

function draw() {
  //ray
  fill(110,120,180,60); //60 defines the opacity of the fill
  stroke(240,240,240,20); //20 defines the opacity of the fill
  strokeWeight(5);
  triangle(300,300,0,600,600,600);
  //UFO
  noStroke();
  fill(40);
  ellipse(300, 340, 250, 100);
  ellipse(300, 300, 130, 130);
  //Window frame
  fill('grey');
  ellipse(300, 300, 85, 85);
  //Windows
  fill(70,130,180);
  ellipse(300, 300, 80, 80);
  //Alien
  fill(50,205,50);
  ellipse(300, 300, 70, 70);
  //face
  fill(0);
  ellipse(285, 295, 10, 20); //left
  ellipse(315, 295, 10, 20); //right
  ellipse(300, 320, 10, 3); //mouth
  // Stars
  let x = random(width);
  let y = random(height);
  textSize(10); //Star size
  text('⭐', x, y);
}
