let colors1 = [[251, 220, 52],[51,153,255],[255,51,51],[102,204,0],[255,128,0],[255,102,178]];
let colors2 = [[251, 220, 52],[51,153,255],[255,51,51],[102,204,0],[255,128,0],[255,102,178]];

function setup() {
  createCanvas(900, 600);
  frameRate(5); 
  background(255);
}

function draw() {
  // Speech bubble
  noStroke()
  fill(135,206,250); 
  rect(70, 170, 760, 290,70);
  triangle(300, 460, 370, 460, 335, 520);

  // Smileys with shadow
  drawShadow();
  let chosenColor1 = random(colors1);
  fill(chosenColor1);
  ellipse(width / 3.5, height / 1.9, 250, 250);

  let chosenColor2 = random(colors2);
  fill(chosenColor2);
  ellipse(width / 1.5, height / 1.9, 250, 250);

  noShadow();
  //Smiley left
  //tears for eyes
  fill('lightblue')
  ellipse(width / 4.1, height / 1.96, 60, 60); //1
  ellipse(width / 3, height / 1.96, 60, 60); //2
  //eye 1
  fill(0);
  ellipse(width / 4.1, height / 2, 60, 60);
  //eye 2
  ellipse(width / 3, height / 2, 60, 60);
  //white blobs
  fill(255)
  ellipse(width / 4.3, height / 2.05, 30, 30); //1 big
  ellipse(width / 3.1, height / 2.05, 30, 30); //2 big
  ellipse(width / 4, height / 1.9, 10, 10); //1 small
  ellipse(width / 2.9, height / 1.9, 10, 10); //2 small
  //sad mouth
  noFill()
  strokeWeight(8);
  stroke(0)
  arc(260,380,30,30,PI,0);

  //Smiley right
  //eye 1
  arc(560,300,30,30,PI,0);
  //eye 2
  arc(630,300,30,30,PI,0);
  //eyebrow 1
  arc(550,270,45,15,PI,0);
  //eyebrow 2
  arc(640,270,45,15,PI,0);
  //mouth
  arc(600,350,30,30,0,PI);
  //blush
  fill(220,20,60,60)
  noStroke()
  ellipse(540,315,45, 30);
  ellipse(650,315, 45, 30);
}
// Shadows for the ellipses
function drawShadow() {
  drawingContext.shadowOffsetX = 5;
  drawingContext.shadowOffsetY = -5;
  drawingContext.shadowBlur = 10;
  drawingContext.shadowColor = 'black';
}
// No shadow rectangle and features
function noShadow() {
  drawingContext.shadowOffsetX = 0;
  drawingContext.shadowOffsetY = 0;
  drawingContext.shadowBlur = 0;
}

