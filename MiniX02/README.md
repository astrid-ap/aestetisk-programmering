Link to view my MiniX02: https://astrid-ap.gitlab.io/aestetisk-programmering/MiniX02

Link to my code: https://gitlab.com/astrid-ap/aestetisk-programmering/-/blob/main/MiniX02/sketchminix02.js?ref_type=heads

### Describe your program and what you have used and learnt.

- colors1 and colors2 are arrays containing six different colors in RGB format used for the smiley faces.

Drawing:
draw function:
Speech bubble:

- Draws a light blue rectangle with rounded corners for the speech bubble.

- Draws a white triangle at the bottom of the speech bubble.

Smileys with shadow:

- Calls the drawShadow function to enable shadows.

- Chooses a random color from colors1 and fills an ellipse for the first smiley face.

- Chooses a random color from colors2 and fills an ellipse for the second smiley face.

- Calls the noShadow function to disable shadows for drawing smiley features.

Smiley on the left (crying):

- Draws two light blue ellipses for tears.

- Draws two black ellipses for eyes.

- Draws two white ellipses (one large, one small) for highlights on each eye.

- Draws a black arc for a sad mouth.

Smiley on the right (happy):

- Draws two black arcs for closed eyes.

- Draws two black arcs for eyebrows.

- Draws an arc for a smiling mouth.

- Draws two pink ellipses for blush.

Shadow functions:

drawShadow function:

- Enables shadows by setting properties of the drawing context:

- shadowOffsetX: Sets the horizontal offset of the shadow (5 pixels to the right).

- shadowOffsetY: Sets the vertical offset of the shadow (5 pixels down).

- shadowBlur: Sets the blur radius of the shadow (10 pixels).

- shadowColor: Sets the color of the shadow (black).

noShadow function:
- Disables shadows by setting the shadow properties back to zero.

Through this program, I have explored various functionalities of p5.js such as drawing shapes, arcs, working with colors, positioning elements, and creating visual effects like shadows!

![](MiniX02video.mov)

### How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, and so on?
Emojis, as discussed in the assigned reading, are not merely typographic symbols but carry deeper meanings and implications. They can oversimplify and universalize differences, continuing normative ideologies within society's power structures. My decision to use a range of colors for my smileys that are modelled after 2 of my favorite emojies challenges the notion of a single neutral skin tone:

Traditional emojis often use yellow as a neutral skin tone, intending to avoid representing any specific race. However, this "neutral" color can still carry implicit biases, suggesting that neutrality itself is a construct influenced by cultural norms.
By using a range of colors ([[251, 220, 52],[51,153,255],[255,51,51],[102,204,0],[255,128,0],[255,102,178]]), my smileys challenge the idea of a single neutral color, reflecting the diversity and multiplicity of identities in the real world, suggesting that no single color can represent everyone.

Also in relation to " "Modifying the Universal", the text critiques the Unicode Consortium's decision to introduce skin tone modifiers as a response to demands for diversity in emoji representation. It highlights the problematic nature of defining a "normal" baseline against which diversity is measured, and questions the use of the Fitzpatrick scale as a basis for skin tone representation. The text argues that representing diversity as mere variants oversimplifies the complexity of identities and their representational needs and suggests that the Unicode Consortium's approach fails to address more nuanced gender identities and cultural representations.



### References
https://p5js.org/reference/#/p5/arc

https://p5js.org/reference/#/p5/drawingContext

https://p5js.org/reference/#/p5/ellipse

https://p5js.org/reference/#/p5/random 

https://p5js.org/reference/#/p5/rect

Winnie Soon & Geoff Cox; 'Aesthetic Programming: A Handbook of Software Studies', 2020

Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51.
